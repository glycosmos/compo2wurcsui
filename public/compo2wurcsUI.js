/*const userAction = async () => {
const response = await fetch('https://ncompo2wurcs.glycosmos.org/Ncomp2wurcs?hex='+hex+'&hexnac='+hexnac+'&fuc='+deoxyhex+'&neuac='+neuac+'&neugc='+neugc+'&core='+core);
const myJson = await response.json(); //extract JSON from the http response
// do something with myJson
var obj = JSON.parse(myJson);
return obj.WURCS;
}
return userAction();
*/
//import  saveAs  from 'file-saver';


//const isTesting = false;
var compoData = []; //[{wurcs:"wurcs string"},{wurcs:"second wurcs"}];
//const proxyurl = "https://cors-anywhere.herokuapp.com/";
const proxyurl = "";
var inputfilename;

document.getElementById('file-input').addEventListener('change', readSingleFile, false);

function removeExtension(filename){
    var lastDotPosition = filename.lastIndexOf(".");
    if (lastDotPosition === -1) return filename;
    else return filename.substr(0, lastDotPosition);
}

function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename?filename+'.xls':inputfilename+'.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }
}

function readSingleFile(e) {
    let file = e.target.files[0];
    if (!file) {
        return;
    }
    inputfilename = removeExtension(file.name);
    let reader = new FileReader();
    reader.onload = function (e) {
        let contents = e.target.result;
        // Display file content
        let lines = contents.split('\n');
        //displayContents(contents);
        getWURCSData(lines);
    };
    reader.readAsText(file);

}



function storeWURCS(wurcs) {
    var wurcsDict = {};
    wurcsDict["wurcs"] = wurcs;
    compoData.push(wurcsDict);
}


function convertArrayOfObjectsToTSV(args) {
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || '\t';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function downloadTSV(args) {
    var data, filename, link;
    //console.log("entered downloadTSV");

    var tsv = convertArrayOfObjectsToTSV({
        data: compoData
    });
    if (tsv == null) {
        console.log("tsv is null");
        return;
    }

    filename = args.filename || inputfilename+'.tsv';

    if (!tsv.match(/^data:text\/tsv/i)) {
        tsv = 'data:text/tsv;charset=utf-8,' + tsv;
    }
    data = encodeURI(tsv);

    //console.log("encoded data "+tsv);

   // var blob = new Blob(["Hello, world!"], {type: "text/tsv;charset=utf-8,"+tsv});
    //FileSaver.saveAs(blob, filename);

    link = document.createElement('a');

    link.setAttribute('href', data);
    link.innerText = "Click to download unregistered WURCS strings as TSV file called 'export.tsv'.";
    link.setAttribute('download', filename);
    //oSaveBtn.Attributes.Add(CNTRATTR_ONCLICK, "onSaveAs();")
    document.body.appendChild(link);
    tblElement = document.getElementById("compotable");
    document.body.insertBefore(link,tblElement);

}

function getWURCSData(lines) {

    let keynames = ["Hex", "HexNAc", "DeoxyHexose", "Neu5Ac", "Neu5Gc", "core", "P", "HSO3", "Ac"];
    let querydatalist = [];


    for (var i = 0; i < lines.length; i++) {
        let querydata = {};
        //console.log("Line "+i+" is : "+lines[i]);
        let columns = lines[i].split('\t');
        //if (columns.length == keynames.length) {
        if (columns.length > 1) {

            for (var j = 0; j < columns.length; j++) {


                /*// obtain WURCS, assuming the order of bases is hex, hexnac, deoxyhex, neuac, neugc, core (0 or 1), p, s, ac
                    */
                let parsed = Number.parseInt(columns[j], 10);
                if (Number.isInteger(parsed)) {
                    querydata[keynames[j]] = columns[j];
                    //console.log("Setting "+keynames[j]+" to "+columns[j]);

                } else {
                    // TODO: parsed number is not integer
                    //console.log("ERROR: could not parse " + columns[j]);
                }
            }
            if (Object.keys(querydata).length > 0) {
                //console.log("Pushing "+querydata);
                querydatalist.push(querydata);
            }
        } else {
            // TODO: keynames length != columns length
            //console.log("ERROR: Number of columns unexpected ");
            //console.log(columns.length);
            //console.log(keynames.length);
        }
    }

    //console.log(JSON.stringify(querydatalist));

    const myurl = "https://ncompo2wurcs.glycosmos.org/Ncomp2wurcs";

    const Http = new XMLHttpRequest();
    Http.open("POST", myurl, true);
    Http.withCredentials = true;

    Http.send(JSON.stringify(querydatalist));

    Http.onload = function() {
        //console.log("onload called with Http readystate="+Http.readyState+" and status "+Http.status);
        var returndata = JSON.parse(Http.responseText);
        if (Http.readyState == 4 && (Http.status == "201" || Http.status == "200")) {
            //console.log("Response text is "+Http.responseText);
            displayContents(returndata);

        }
    }


}


function displayContents(contents) {

    //console.log("DisplayContents got data "+contents);
    //console.log("Results="+contents['results']);
    let body = document.getElementsByTagName('body')[0];


    let element = document.getElementById('file-content');
    //element.innerHTML = contents;

    body.innerHTML = '';

    if (getUrlVars()["test"] == "true") {
        body.innerHTML = "<h1>Test mode</h1>";
    } else {
        body.innerHTML = "<h1>Composition to WURCS/GlyTouCan ID Results</h1> <button onclick=\"exportTableToExcel('compotable')\">Export Table Data To Excel File</button>";

    }

    let keynames = ["Hex", "HexNAc", "DeoxyHexose", "Neu5Ac", "Neu5Gc", "core", "P", "HSO3", "Ac", "WURCS", "id"];

    // remove unnecessary keynames
    for(var key in keynames){
        if (!key in contents['results']) {
            var index = keynames.indexOf(key);
            if (index > -1) {
                keynames.splice(index,1);
            }
        }
    }

    let tbl = document.createElement('table');
    tbl.id = "compotable";
    tbl.style.width = '100%';
    tbl.style.height = '100%';
    tbl.setAttribute('border', '1');

    var thead = document.createElement('thead');

    tbl.appendChild(thead);

    for(var i=0;i<keynames.length;i++){
        thead.appendChild(document.createElement("th")).
        appendChild(document.createTextNode(keynames[i]));
    }

    let tbdy = document.createElement('tbody');
    //let lines = contents.split('\n');

    for (var i = 0; i < contents['results'].length; i++) {
        let tr = document.createElement('tr');
        //let columns = lines[i].split('\t');
        if (Object.keys(contents['results'][i]).length > 1) {
            //console.log("Contents of "+i+ " is "+JSON.stringify(contents['results'][i]));
            for (var key of keynames) {
                console.log("Key = "+key);
                let td = document.createElement('td');
                if (contents['results'][i][key] != undefined) {

                    td.appendChild(document.createTextNode(contents['results'][i][key]));
                    if (key == "id" && contents['results'][i][key].length == 0) {
                        //console.log("Key is empty ID, storing WURCS " + contents['results'][i]['WURCS']);
                        storeWURCS(contents['results'][i]['WURCS']);
                    }
                }
                tr.appendChild(td);
            }
            tbdy.appendChild(tr);
        }
    }
    tbl.appendChild(tbdy);

    body.appendChild(tbl);





    if (compoData.length > 0) {
        downloadTSV("");
    }


}

