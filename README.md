# compo2wurcsUI

A simple Web interface that takes a tab-delimited text file of monosaccharide
base compositions.  Select the file on the local computer containing such data
(see sampledata.txt), and a table of all GlyTouCan IDs corresponding to the
base compositions will display.  If the given composition is not registered,
it will display the WURCS sequence.  This will allow the user to simply use
the WURCS sequence to register their compositions.

## How it works

Select a file on the local computer containing a tab-delimited text of compositions as follows:

<pre>
hex	hexnac	deoxyhex	neuac	neugc	core	P	S	Ac
1	2	3	1	0	0	1	1	1
1	2	3	1	0	1	0	0	0
2	1	0	0	0	0	1	0	0
2	1	1	2	1	0	0	1	0
2	1	1	2	1	1	0	0	1
</pre>

Then, if successful, a table of the same list of compositions will be displayed, with an 
additional column at the right end to list either GlyTouCan IDs or WURCS sequences.
If no GlyTouCan ID exists for the composition, then the WURCS sequence is displayed
providing the user the opportunity to copy the WURCS sequence and register it in GlyTouCan.

Moreover, a link to download the list of WURCS sequences (if any) will be displayed above
the table, to allow the user to register the list of sequences in batch into GlyTouCan.

## Troubleshooting
If the table as described above is not displayed, then the server to obtain WURCS strings
or GlyTouCan IDs may be down.  In this case, please try the tool again later.
As another possible cause of error, there may be a cross-origin
server error.  Depending the browser being used, various workaround are possible:

* On Safari, this security setting can be disabled by going to the Advanced
Preferences and making sure that "Show Develop in menu bar" is checked.  Then under
the Develop menu, make sure that "Disable cross-origin restrictions" is checked.
* On Chrome, all running Chrome windows need to be closed and then reexecuted using
the following command at the command line (on Mac OS):

       $ open -a Google\ Chrome --args --allow-file-access-from-files